//
// NOPTEL Laser connection
//

var laserOn = true;


//------------------ Noptel connection --------------

// Socket connection to Noptel laser range finder
var net = require('net');
const noptel = new net.Socket();

noptel.connect(10001, '192.168.0.15', function() {
    console.log('Noptel connected');
});

noptel.on('data', function(data) {
//	console.log("" + data);
        if (socket) {
  	    console.log('' + data);
            socket.emit('message', '' + data);
        }
	
	noptel.write('\x1Bc1\n');
});

noptel.on('close', function() {
	console.log('Connection closed');
});

// Toggle the red sighting laser on and off
setInterval (function () {
    if (laserOn) {
	noptel.write('\x1BO1\n');
        laserOn = false;
    } else {
	noptel.write('\x1BO0\n');
        laserOn = true;
    }
}, 500);


//------------------ Web socket to server --------------
const io = require('socket.io-client');

const wsurl =  'http://xn--2-umb.net/ws/terminal';
console.log('Attempting connect to websocket at:' , wsurl);
const socket = io.connect(wsurl);

socket.on('connect', () => {
    console.log('Web socket connected to:', wsurl);
});

socket.on('error', (err) => {
    console.log('Web socket error:', err);
});



