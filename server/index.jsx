/*jslint node: true */
'use strict';

////////////////////// Debugging aids ////////////////////////////////////////

process.on('SIGUSR2', function() {
    console.log('Got SIGUSR2. I\'ll go collect the garbage');
    global.gc();
});

////////////////////// Stormpath Authentication Setup //////////////////////////
// Create a stormpath client
import stormpath from 'stormpath';
var client = null;
var homedir = (process.platform === 'win32') ? process.env.HOMEPATH : process.env.HOME;
//var keyfile = '/home/ec2-user/.stormpath/apiKey.properties';
var keyfile = '/home/michael/.stormpath/apiKey.properties';

let stormpathApp;
stormpath.loadApiKey(keyfile, function apiKeyFileLoaded(err, apiKey) {
    if (err) throw err;
    let stormpathClient = new stormpath.Client({apiKey: apiKey});
    console.log('Stormpath client created OK.')

    // Retreive stormpath application
    stormpathClient.getApplications({name:'propanel'}, function(err, applications){
        if (err) throw err;

        stormpathApp = applications.items[0];
        console.log('Stormpath application retrieved OK.')

        // Create a user for 'guest' logins
        var account = {
            givenName: 'guest',
            surname:   'guest',
            username:  'guest',
            email:     'guest@xn--2-umb.net',
            password:  'Password1',
            customData: {
                favoriteColor: 'white',
            }
        };
        stormpathApp.createAccount(account, function(err, account) {
          if (err) {
              console.log('Stormpath create guest user FAIL', err.userMessage);
          } else {
              console.log('Stormpath create guest user OK');
          }
        });
    });
});

////////////////////// Web Server //////////////////////////////////////////////
import fs from 'fs';
let hskey  = fs.readFileSync('test-key.pem');
let hscert = fs.readFileSync('test-cert.pem');

let options = {
    key:  hskey,
    cert: hscert
};

import https from 'https';
import express from 'express';
let app = express();
let server = https.createServer(options, app);

import websocket from './websocket.jsx';
import uuid from 'node-uuid';
var jwtSecret = uuid.v4();
websocket(server, jwtSecret);

// Implement security policies with Helmet
import helmet from 'helmet';

// FIXME: Horrible hardwired host:port here !
// Get host name and port to listen on from environment
const HOSTNAME = process.env.PROPANEL_HOSTNAME   || 'localhost';
const PORT     = process.env.PROPANEL_HTTPS_PORT || 443;

// Configure Content Security Policy
app.use(helmet.csp({
    defaultSrc:  ["'self'"],
    scriptSrc:   ["'self'"],
    styleSrc:    ["'self'", "'unsafe-inline'"],
    imgSrc:      ["'self'"],
    connectSrc:  ["'self'",
                  "wss://xn--2-umb.net",   // DOES NOT WORK IN FIREFOX.
                  "wss://*.net",
                  "wss://localhost"
                 ],
    fontSrc:     ["'self'"],
    objectSrc:   [],
    mediaSrc:    [],
    frameSrc:    []
    // TODO: CSP Violation reporting
}));

// Implement X-XSS-Protection
app.use(helmet.xssFilter());

// Implement X-Frame: Deny
app.use(helmet.xframe('deny'));

// Implement Strict-Transport-Security
// Note: This does not work unless we actually use HTTPS (req.secure = true)
app.use(helmet.hsts({
    maxAge: 7776000000,      // 90 days
    includeSubdomains: true
}));

// Hide X-Powered-By
app.use(helmet.hidePoweredBy());

import bodyParser from 'body-parser';
app.use(bodyParser.json({}));

import passport from 'passport';
app.use(passport.initialize());

import csrf from 'csurf';
// FIXME: This is disabled for testing
//app.use(csrf());

server.listen(PORT, function () {
    console.log(`https://${HOSTNAME}:${PORT}`);
});

// Compress served content
import compression from 'compression';
app.use(compression());

import favicon     from 'serve-favicon';
app.use(favicon('icons/favicon.ico'));

// This gets webpack hot loading working
import webpack              from 'webpack';
import webpackConfig        from '../webpack.config';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

// Step 1: Create & configure a webpack compiler
const compiler = webpack(webpackConfig[0]);

// Step 2: Attach the dev middleware to the compiler & the server
app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: webpackConfig[0].output.publicPath
}));

// Step 3: Attach the hot middleware to the compiler & the server
app.use(webpackHotMiddleware(compiler, {
    log: console.log,
    path: '/__webpack_hmr',
    heartbeat: 10 * 1000
}));

import cors        from 'cors';
app.use(cors());

// Serve static content from the public subdirectory
//const staticPath = __dirname;
const staticPath = "./";
console.log('Serving static content from:', staticPath);
import serveStatic from 'serve-static';
app.use(serveStatic(staticPath));

// Let's make our express `Router` first. (This is just for checking winston error logging)
var router = express.Router();

var JwtStrategy = require('passport-jwt').Strategy;
var opts = {}
opts.secretOrKey = jwtSecret;
//opts.issuer = 'xn--2-umb.net';
//opts.audience = 'xn--2-umb.net';
opts.tokenBodyField = 'token';
opts.tokenQueryParameterName = 'token';

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    // This function only called if JWT is valid.
    return done(null, jwt_payload);
/*
    User.findOne({id: jwt_payload.sub}, function(err, user) {
        if (err) {
            return done(err, false);
        }
        if (user) {
            done(null, user);
        } else {
            done(null, false);
            // or you could create a new account
        }
    });
*/
}));

// Passport authentication with a custom custom callback
// Store JWT in cookies for extra secrity. See here:
// https://github.com/themikenicholson/passport-jwt/issues/28
// N.B. We are not using cookies for seesion management.
import jwt from 'jsonwebtoken';
import Cookies from 'cookies';

app.get('/', function(req, res, next) {
    console.log("Authenticating with JWT");

    var token = new Cookies(req, res).get('access_token');
    if (token) {
        try {
            var verifiedJwt = jwt.verify(token, jwtSecret);
            // Stuff cookies access_token into request body for passport.jwt to find
            req.body.token = token;
        } catch (e) {
            console.log('jwt.verify failed:', e);
        }
    }

    passport.authenticate('jwt', function(err, jwt_payload) {
        console.log('jwt_payload', jwt_payload);
        if (err) {
            console.log('passport.authenticate ERROR: ', err);
            return next(err);
        }
        if (jwt_payload === false) {
            console.log("JWT Authentication: FAIL");
            return res.redirect('/login');
        }
        console.log("JWT Authentication: OK", jwt_payload);
        next();
    })(req, res, next);
});

// Passprt authentication as middleware
app.get('/fish', passport.authenticate('jwt', { session: false}),
    function(req, res) {
        console.log("app.get /fish");
        res.send("Fish here.");
    }
);

router.get('/error', function(req, res, next) {
    // Here we cause an error in the pipeline so we see express-winston in action.
    return next(new Error("This is an error and it should be logged to the console"));
});

app.post('/login', function (req, res) {
    // TODO: validate the user

    // Authenticate with stormpath using username and password
    let account;
    stormpathApp.authenticateAccount({
        username: req.body.username,
        password: req.body.password,
    }, function (err, result) {
        if (err) {
            console.log('stormpath authentication FAIL', err);
            res.status(418).send('Incorrect username or password'); // I'm a tea pot
        } else {
            console.log('stormpath authentication OK', result);
            account = result.account;
            console.log('Got account: ', result.account);

            // See here: https://stormpath.com/blog/nodejs-jwt-create-verify/
            let profile = {
                username: 'guest',
                email:    'guest@xn--2-umb.net',     // FIXME: Use and Id here not username/email!
                id:       123
            };

            // Set the profile in the token
            let token = jwt.sign(profile, jwtSecret, { expiresIn : 60 * 60 });
            console.log('token:::::::::::::::::', token);

            // Send the token in a cookie
            new Cookies(req,res).set('access_token', token, {
                httpOnly: true,     // Javascript cannnot read cookie
                secure:   true      // Send over HTTPS only for your production environment
            });

            // Also send the token in the body FIXME: remove this.
            res.json({token: token});
        }
    });
});

import pw from 'credential';
router.post('/api/register', function (req, res) {
    var account = {
        username:  req.body.username,
        email:     req.body.email,
        password:  req.body.password,
        givenName: 'unkown',
        surname:   'unkown',
        customData: {
            favoriteColor: 'white',
        }
    };
    stormpathApp.createAccount(account, function(err, account) {
        if (err) {
            console.log('stormpath account creation failed', err);
            res.status(418).send(err.message); // I'm a tea pot
        } else {
            res.status(200).send({});
        }
    });
});

import expressWinston from 'express-winston';
import winston from 'winston';

// express-winston logger makes sense BEFORE the router.
app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console({
            json: true,
            colorize: true
        })
    ]
}));

// Now we can tell the app to use our routing code:
app.use('/', router);

import React                   from 'react';
import {RoutingContext, match} from 'react-router';
import ReactDOMServer          from 'react-dom/server';
import routes                  from '../app/routes.jsx';

function injector() {
    console.log("Server side injector here!");
}

// All other requests come here. Put this last.
app.use((req, res) => {
    try {
        // For info on how routing works:
        // https://github.com/rackt/react-router/blob/master/docs/guides/advanced/ServerRendering.md
        const customProps = {
            /*Custom Data*/
            injector: injector
        };

        // Note that req.url here should be the full URL path from
        // the original request, including the query string.
        match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
            if (error) {
                res.status(500).send(error.message);
            } else if (redirectLocation) {
                res.redirect(302, redirectLocation.pathname + redirectLocation.search);
            } else if (renderProps) {
                const routerProps = {
                    ...renderProps,
                    createElement: (Component, props) => {
                        return React.createElement(Component, {...props, ...customProps});
                    }
                }
                let factory = React.createFactory(RoutingContext)(routerProps);
                res.status(200).send(ReactDOMServer.renderToString(factory));
            } else {
                res.status(404).send('Not found');
            }
        });
    } catch (e) {
        console.log('----------------- ROUTER FAILURE! ---------------------------');
        console.log(e);
    }
});

// express-winston errorLogger makes sense AFTER the router.
app.use(expressWinston.errorLogger({
    transports: [
        new winston.transports.Console({
            json: true,
            colorize: true
        })
    ]
}));

/*
// Optionally you can include your custom error handler after the logging.
app.use(express.errorLogger({
    dumpExceptions: true,
    showStack: true
}));
*/
