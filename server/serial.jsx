////////////////////// Serial Port Handling ///////////////////////////////////

import serialport from 'serialport';

const SerialPort = serialport.SerialPort;

let serialPortStatus = 'none';

// Get serial port to use from environment
const SERIAL_PORT = process.env.PROPANEL_SERIAL_PORT || '/dev/ttyAMA0';
const sp = new SerialPort(SERIAL_PORT, {
    baudrate: 115200,
    parser:   serialport.parsers.readline('\n')
});

function handleSerial(line) {
    // Incomming lines of data will need parsing here.
    // For test we just send the line to the web panel terminal window.
    broadcast('terminalMessage', line);
}

sp.on('open', function () {
    console.log('Serial port open.');
    serialPortStatus = 'open';
    sp.on('data', function (line) {
        console.log('Serial data: ', line);
        handleSerial(line);
    });
});

sp.on('error', function (err) {
    serialPortStatus = err;
    console.log(err);
});

// Writes data and waits until it has finish transmitting to the target serial port before calling the callback.
function writeAndDrain(data, callback) {
    sp.write(data, function () {
        sp.drain(callback);
    });
}
