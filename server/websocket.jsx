//
// Web Socket Server
//

import listen from 'socket.io';
import socketioJwt from 'socketio-jwt';

function websocket(server, jwtSecret) {
    const sio = listen(server);

/*
    sio.set('authorization', socketioJwt.authorize({
        secret: jwtSecret,
        handshake: true
    }));
*/
    const terminal = sio
        .of('/ws/terminal')
        .on('connection', function (socket) {
            console.log('Terminal connected');
            socket.on('message', function (data) {
                terminal.emit('message', data);
            });
        });

    // Noptel laser range finder websocket
    const noptel = sio
        .of('/ws/noptel')
        .on('connection', function (socket) {
            console.log('Noptel connected');
            socket.on('message', function (data) {
                console.log(message);
            });
        });

    sio.set('origins', '*:*');

    const clientStore = {};
    let clientCount = 0;

    let ledState    = [false, false, false, false, false, false, false, true];
    let buttonState = [false, false, false, false, false, false, false, true];

    function broadcast(event, data) {
        let client;
        for (client in clientStore) {
            if (clientStore.hasOwnProperty(client)) {
                clientStore[client].emit(event, data);
            }
        }
    }

    function rememberClient(socket) {
        clientStore[socket.id] = socket;
        clientCount += 1;
    }

    function forgetClient(socket) {
        delete clientStore[socket.id];
        clientCount -= 1;
    }

    // AcceptConnections
    sio.on('connection', function (socket) {
        rememberClient(socket);

        broadcast('logMessage', Date() + ': websocket connect: ' + socket.id);
        broadcast('logMessage', Date() + ': Connected clients: ' + clientCount);
        //broadcast('logMessage', Date() + ': Serial port status: ' + serialPortStatus);

        broadcast('ledState', ledState);
        broadcast('buttonState', buttonState);

        socket.on('pong', function (data) {
            console.log(data);
            return data;
        });

        socket.on('buttonState', function (data) {
            console.log(data);
            buttonState = data;
            ledState = data;
            broadcast('buttonState', buttonState);
            broadcast('ledState', buttonState);
            broadcast('logMessage', Date() + ': Buttons: ' + buttonState);
        });

        socket.on('disconnect', function () {
            forgetClient(socket);
            broadcast('logMessage', Date() + ': websocket connect: ' + socket.id);
            broadcast('logMessage', Date() + ': Connected clients: ' + clientCount);
        });
    });

    ////////////////////// "ping" messages over websocket //////////////////////////

    setInterval(function sendPing() {
        broadcast('ping', { ping: 'Ping!' });
    }, 1000);

    ////////////////////// Test messages over websocket ////////////////////////////

    function signal() {
        const t = Date.now() / 500;
        let s = 0;

        if (buttonState[0]) {
            s = Math.sin(t);
        }
        if (buttonState[1]) {
            s += Math.sin(t * 3) / 3;
        }
        if (buttonState[2]) {
            s += Math.sin(t * 5) / 5;
        }
        if (buttonState[3]) {
            s += Math.sin(t * 7) / 6;
        }
        if (buttonState[4]) {
            s += Math.sin(t * 9) / 9;
        }
        if (buttonState[5]) {
            s += Math.sin(t * 13) / 13;
        }
        if (buttonState[6]) {
            s += Math.sin(t * 15) / 15;
        }
        if (buttonState[7]) {
            s += Math.sin(t * 16) / 16;
        }
        s *= 256;
        return s;
    }

    setInterval(function sendAnalog() {
        broadcast('analog', signal());
    }, 100);
}

////////////////////// An experiment in CSP ////////////////////////////////////

import csp from 'js-csp';

function * player(name, table) {
    while (true) {
        var ball = yield csp.take(table);
        if (ball === csp.CLOSED) {
            // console.log(name + ": table's gone");
            return;
        }
        ball.hits += 1;
        // console.log(name + " " + ball.hits);
        yield csp.timeout(100);
        yield csp.put(table, ball);
    }
}

csp.go(function * () {
    var table = csp.chan();

    csp.go(player, ['ping', table]);
    csp.go(player, ['pong', table]);

    yield csp.put(table, {hits: 0});
    yield csp.timeout(100000);
    table.close();
});

module.exports = websocket;
