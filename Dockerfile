# This image has node.js installed and the camera setup done. 
FROM nghiant2710/armv7-rpi2-node

# This starts a systemd service so that web terminal access is
# possible even if the node process crashes.
ENV INITSYSTEM on

# Put node package file into root of container
COPY package.json /package.json

# Install required node packages
RUN npm install

# Copy sources into the containers app directory
COPY src/ /app

# Start the propanel server
#CMD ["npm", "start"]
CMD ["node", "/app/server.js"]

