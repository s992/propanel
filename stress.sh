#!/bin/bash

host=$(idn2 2π.net)
port=

#host=localhost
#port=1234

#host=52.30.8.159
#port=

while [ true ]
do
    echo "-------------------------------------------------------------------------------"
    echo $(date)
    echo "----------------------------"
    curl -k https://$host:$port/assets/bundle.js > /dev/null
    curl -k https://$host:$port > /dev/null 
    echo "-------------------------------------------------------------------------------"
done

