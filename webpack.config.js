//
// See here:
//    https://github.com/petehunt/webpack-howto
//    http://jlongster.com/Backend-Apps-with-Webpack--Part-I
//
/*jslint node: true */
'use strict';

var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function (x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function (mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });
nodeModules.term = 'commonjs term';

console.log("------------ NODE MODULES EXCLUDED --------------");
console.log(nodeModules);
console.log("-------------------------------------------------");

var publicPath = "/";

module.exports = [
    {
    	// The configuration for the client
        name: 'browser',
        entry: {
            app: ['webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000', './app/browser.jsx'],
        },

        output: {
            path: __dirname + '/assets',
            publicPath: publicPath,
            filename: 'bundle.js'
        },

        devtool: '#source-map',

        devServer: {
            contentBase: "./dev-build"

            // noInfo: true, //  --no-info option
            // hot: true,
            // inline: true
        },

        module: {
            noParse: [
            ],
            loaders: [
                {
                    test: /\.jsx$/,
                    //exclude: /(node_modules)/,  Do we need this
                    loader: "babel",
                    query: {
                        optional: ['runtime', 'es7.objectRestSpread'],
                        stage: 0,
                        cacheDirectory: true
                    }
                },
                {
                    test: /\.css$/,
                    loader: 'style-loader!css-loader'
                },
                {
                    test: /\.(png|jpg|gif|woff|woff2|eot|ttf|svg)$/,
                    loader: 'url-loader?limit=3000000'
                }
            ]
        },

        plugins: [
            // Uglify is slow, should disable for dev
            //new webpack.optimize.UglifyJsPlugin(),
            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoErrorsPlugin()
        ],

        resolve: {
            alias: {
                // We won't be including boostrap in our bundle
                //'bootstrap.css': css_dir + '/bootswatch_com_cyborg_bootstrap.min.css'
            }
        }
    },
    {
        // The configuration for the server-side rendering
		name: "server",
        target: "node",
        entry: "./server/index.jsx",
        output: {
            path: __dirname + '/assets',
            publicPath: publicPath,
            filename: 'server-bundle.js',
            libraryTarget: "commonjs2"
        },

        devtool: '#source-map',

        devServer: {
            contentBase: "./dev-build"

            // noInfo: true, //  --no-info option
            // hot: true,
            // inline: true
        },
        externals: nodeModules,
        module: {
            noParse: [
            ],
            loaders: [
                {
                    test: /\.jsx$/,
                    // exclude: /(node_modules)/,  Do we need this?
                    loader: "babel",
                    query: {
                        optional: ['runtime'],
                        stage: 0,
                        cacheDirectory: true
                    }
                },
                {
                    test: /\.(png|jpg|gif|woff|woff2|eot|ttf|svg)$/,
                    loader: 'url-loader?limit=3000000'
                }
            ]
        },

        plugins: [
            new webpack.IgnorePlugin(/\.(css|less)$/),

            // Automatically sourcemap stack traces from node.js
            new webpack.BannerPlugin('require("source-map-support").install();',
                                       { raw: true, entryOnly: false }),

            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoErrorsPlugin()
        ],

        resolve: {
            alias: {
                // We won't be including boostrap in our bundle
                //'bootstrap.css': css_dir + '/bootswatch_com_cyborg_bootstrap.min.css'
            }
        }
    }
];
