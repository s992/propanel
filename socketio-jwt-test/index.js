var socketioJwt   = require('socketio-jwt');
var jwt           = require('jsonwebtoken');
var express       = require('express');
var http          = require('http');
var Cookies       = require('cookies');
var cookieParser  = require('socket.io-cookie-parser');

var app    = express();
var server = http.Server(app);
var io     = require('socket.io')(server);

server.listen(3001, function () {
    console.log('listening on *:3001');
});

var jwtSecret = 'I am a secret, keep me.';
var profile = {
    username: 'guest',
    email:    'guest@example.com',
    id:       123
};
// Set the profile in the JSON web token
var accessToken = jwt.sign(profile, jwtSecret, { expiresIn : 60 * 60 * 24});

app.get('/', function (req, res) {
    // Send the token in a cookie
    new Cookies(req,res).set('accessToken', accessToken, {
        httpOnly: true,      // Javascript cannnot read cookie
        secure:   false      // Send over HTTPS only for your production environment
    });
    res.sendFile(__dirname + '/index.html');
});

io.use(cookieParser());

var accessTokenNow = null;

io.use(function authorization(socket, next) {
    // cookies are available in:
    // 1. socket.request.cookies
    // 2. socket.request.signedCookies (if using a secret)
    accessTokenNow = socket.request.cookies.accessToken;
    next();
});

io.of('/ws/chat')
.on('connection', socketioJwt.authorize({
    secret:  jwtSecret,
    timeout: 15000,           // 15 seconds to send the authentication message
    getToken: function () {
        return accessTokenNow;
    }
}))
.on('authenticated', function (socket) {
    console.log('Chat connected & authenticated: ', socket.decoded_token);
    socket.on('chat message', function (data) {
        console.log('Chat message:', data);
        socket.emit('chat message', data);
    });
});
