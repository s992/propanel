# propanel

A demo of building a web app out of react.js components.

A few highlighs:

* Uses react.js for easy component building
* d3.js for SVG charts
* Three.js for webgl rendering
* websockets
* bootstrap for styling
* webpack bundler and hot browser reloading during development
* Uses server side rendering by react for supper fast page load.


Install
-------

Propanel requires a recent node.js version. Last tested with v4.2.1

    $ git clone https://zicog@bitbucket.org/zicog/propanel.git
    $ cd propanel
    $ npm install

Develop
-------

Use webpack to build a bundle of all js, css, images etc and run the server:

    $ cd propanel
    $ export PROPANEL_HTTP_PORT=1234
    $ npm start

Point your browser at localhost:1234

This is hot load enabled so changes in client code will trigger a browser reload.

Production
----------

    $ npm run production
    
Will optimize and minify the bundle. A reduction in size of nearly 3 times.




