import React from 'react';
import { Link } from 'react-router';
import { Panel, Grid, Col } from 'react-bootstrap';

import auth from '../auth.jsx';

const Logout = React.createClass({
    componentDidMount() {
        auth.logout()
    },

    render () {
        console.log('Renedering: Logout');
        return (
            <Grid fluid>
                <Col md={6} mdOffset={3}>
                    <Panel header='Logout:'>
                        <p>You are now logged out</p>
                        <Link to='/login'>Login</Link>
                    </Panel>
                </Col>
            </Grid>
        );
    }
})

module.exports = Logout;
