import React from 'react';

function superHero (target) {
    target.isSuperHero = true;
    target.power = 'flight';
}

// Can't do decorators in iojs yet
//@superHero
class Floater extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.style =  {
            position:   'fixed',
            top:        '300px',
            left:       '50px',
            height:     '200px',
            width:      '200px',
            zIndex:     '10',
            background: '#1c1e22'
        };
    }

    render() {
        return (
            <div  className='floater well' style={this.style}>
                This floats in the 3rd dimension.
            </div>
        );
    }
}

module.exports = Floater;
