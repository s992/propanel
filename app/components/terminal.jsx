import React    from 'react';
import reactDOM from 'react-dom';
import io       from 'socket.io-client';

import Term     from './term';
//import auth from '../auth.jsx';

function string2hex (s) {
    var hex, i;

    var result = "";
    for (i=0; i < s.length; i++) {
        hex = s.charCodeAt(i).toString(16);
        result += ("000"+hex).slice(-4);
    }
    return result
}


class Terminal extends React.Component {
    constructor(props) {
        super(props);
        // The VT100 terminal emulation.
        const colors = [
          '#000000', // black
          '#cd0000', // red3
          '#00cd00', // green3
          '#cdcd00', // yellow3
          '#0000ee', // blue2
          '#cd00cd', // magenta3
          '#00cdcd', // cyan3
          '#e5e5e5', // gray90
          // bright:
          '#7f7f7f', // gray50
          '#ff0000', // red
          '#00ff00', // green
          '#ffff00', // yellow
          '#5c5cff', // rgb:5c/5c/ff
          '#ff00ff', // magenta
          '#00ffff', // cyan
          '#ffffff'  // white
        ];
        colors[256] = '#300300'; // Terminal background
        colors[257] = '#ffa500'; // Terminal foreground
        this.state = {};
        this.state.term = new Term({
            cols: 80,
            rows: 24,
            useStyle: true,
            screenKeys: true,
            cursorBlink: false,
            colors: colors
        });
    }

    componentDidMount () {
        // Element, and it's width is only know after rendering

        const el = reactDOM.findDOMNode(this);
        const term = this.state.term;

        term.open(el);
        term.write('\x1b[31mWelcome to Propanel!\x1b[m\r\n');

        console.log('Socket.io connecting terminal...');
        const wsurl = window.location.origin + '/ws/terminal'
        const socket = io.connect(wsurl);
//      const token = auth.getToken();
//      const socket = io.connect(wsurl,  {
//           query: 'token=' + token
//      });
        socket.on('connect', () => {
            console.log('Socket.io authenticated, connected to: terminal ');
            term.write('\x1b[31m\r\nTerminal connected.\x1b[m\r\n');
        });
        socket.on('disconnect', () => {
            console.log('Socket.io disconnected from: terminal ');
            term.write('\x1b[31m\r\nTerminal disconnected.\x1b[m\r\n');
        });

        socket.on('message', (data) => {
            term.write(data);
        });

        term.on('title', (title) => {
            // We could use this for the terminal window title
        });

        term.on('data', (data) => {
            console.log(string2hex(data));
            // Replace del with BS
            var data1 = data.replace(/\u0007f/g, "\u00008");

            // Convert to DOS style line endings
            var data2 = data.replace(/\r/g, "\r\n");

            console.log(string2hex(data2));

            socket.emit('message', data2);
        });
    }

    render() {
        return (
            <div style={{overflow: 'hidden'}}>
            </div>
        );
    }
}

module.exports = Terminal;
