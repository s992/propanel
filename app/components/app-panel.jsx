import React from 'react';


import Immutable from 'immutable';
import io from 'socket.io-client';

import httpRequest     from './ajax.jsx';


class App extends React.Component {
    constructor(props) {
        console.log("App constructor");
        super(props);
        this.state = {
            plot1: Immutable.List([
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 100, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0
            ]),
            plot2: Immutable.List([
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 200, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0
            ]),
            switches: Immutable.List([
                {buttonId: 0, label: 'GPIO  0', button: 'on',  led: 'on' },
                {buttonId: 1, label: 'GPIO  1', button: 'off', led: 'off'},
                {buttonId: 2, label: 'GPIO  2', button: 'off', led: 'off'},
                {buttonId: 3, label: 'GPIO  3', button: 'off', led: 'off'},
                {buttonId: 4, label: 'GPIO  4', button: 'on',  led: 'off'},
                {buttonId: 5, label: 'GPIO  5', button: 'off', led: 'off'},
                {buttonId: 6, label: 'GPIO  6', button: 'off', led: 'off'},
                {buttonId: 7, label: 'GPIO  7', button: 'off', led: 'off'}
            ]),
            tabKey: 1
        };
    }

    updatePlot(data) {
        const plot = this.state.plot.push(data);
        if (plot.size > 64) {
            this.setState({plot: plot.shift()});
        } else {
            this.setState({plot: plot});
        }
    }

    componentWillMount () {

        const socket = io.connect(window.location.origin);
        socket.on('ping', () => {
            socket.emit('pong', { pong: 'Pong' });
        });

        socket.on('analog', (data) => {
            //updatePlot(data);
        });

        socket.on('ledState', (data) => {
            console.log("Got : LEDs");
            data.map((ledState, index) => {
                const s = this.state.switches.get(index);
                s.led = ledState ? 'on' : 'off';
                this.setState({switches: this.state.switches.set(index, s)});
            });
        });

        socket.on('buttonState', (data) => {
            console.log("Got : Buttons");
            data.map((buttonState, index) => {
                const s = this.state.switches.get(index);
                s.button = buttonState ? 'on' : 'off';
                this.setState({switches: this.state.switches.set(index, s)});
            });
        });
    }

    onClick(eventKey) {
        console.log('CLICKED');

        const s = this.state.switches.get(eventKey);
        console.log(s.button);
        s.button = s.button === 'on' ? s.button = 'off' : s.button = 'on';
        this.setState({switches: this.state.switches.set(eventKey, s)});

    }

    handleTabSelect(tabKey) {
        //alert('selected ' + tabKey);
        this.setState({tabKey});
    }

    render () {
    }
}

module.exports = App;
