import React from 'react';
import {form, Input, option, ButtonInput, Panel, Grid, Col } from 'react-bootstrap';
import request from 'superagent';

class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: false,
            message: ''
        }
    }

    handleSubmit (event) {
        event.preventDefault();

        const username  = this.refs.username.getValue();
        const email     = this.refs.email.getValue();
        const password1 = this.refs.password1.getValue();
        const password2 = this.refs.password2.getValue();

        if (password1 !== password2) {
            this.setState({ error: true, message: 'Entered passwords do not match'});
            return;
        }
        request
            .post('/api/register')
            .send({ username: username, email: email, password: password1 })
            .set('X-API-Key', 'foobar')
            .set('Accept', 'application/json')
            .end(function (err, res) {
                // Calling the end function will send the request
                console.log('--------------------------------------------------------');
                if (err) {
                    console.log('Registration request FAIL');
                    this.setState({ error: true, message: 'Signup failed: User name already in use' });
                    return;
                }
                console.log('Registration request OK');
                console.log('--------------------------------------------------------');
                this.setState({ error: false, message: 'Signup OK.'});
            }.bind(this));
    }

    render () {
        let response = (
            <div>{this.state.message}</div>
        );

        return (
            <Grid fluid>
                <Col md={6} mdOffset={3}>
                    <Panel header='Sign up:'>
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <Input ref='username'  type='User name' label='User name'     placeholder='User name' />
                            <Input ref='email' type='email'     label='Email Address' placeholder='Enter email' />
                            <Input ref='password1' type='Password'  label='Password' />
                            <Input ref='password2' type='Password'  label='Re-enter password' />
                            <ButtonInput type='submit' value='Sign up' />
                        </form>
                        {response}
                    </Panel>
                </Col>
            </Grid>
        );
    }
}

module.exports = Signup;
