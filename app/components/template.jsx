import React from 'react';
import { Link } from 'react-router';
//import csp from 'js-csp';

class Template extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: 'Component Template.',
            date: '...'
        };
    }

    static contextTypes = {
        isAuthenticated: React.PropTypes.bool
    }

    static defaultProps = {
          aDefaultProp: "aDefaultProp"
    }

    componentWillMount() {
        // Called before the render method is executed. It is important to note
        // that setting the state in this phase will not trigger a re-rendering.
    }

    componentDidMount () {
        // Called as soon as the render method has been executed. The DOM can be accessed in this
        // method, enabling to define DOM manipulations or data fetching operations. Any DOM
        // interactions should always happen in this phase not inside the render method.
        // An experiment in CSP
/*
        console.log('template.componentDidMount');
        csp.go(function*() {
            while (1) {
                yield csp.take(csp.timeout(1000));
                this.setState({date: Date()});
            }
        }.bind(this));
*/
    }

    shouldComponentUpdate(nextProps, nextState) {
        // Return boolean.
        // Access to the upcoming as well as the current props and state ensure that possible
        // changes can be detected to determine if a rendering is needed or not.
        return true;
    }

    componentWillReceiveProps(nextProps) {
        // Only called when the props have changed and when this is not an initial rendering.
        // componentWillReceiveProps enables to update the state depending on the existing and
        // upcoming props, without triggering another rendering. One interesting thing to remember
        // here is that there is no equivalent method for the state as state changes should never
        // trigger any props changes.
    }

    componentWillUpdate(nextProps, nextState) {
         // Called as soon as the the shouldComponentUpdate returned true. Any state changes via
         // this.setState are not allowed as this method should be strictly used to prepare for an
         // upcoming update not trigger an update itself.
    }

    componentDidUpdate(prevProps, prevState) {
        // Called after the render method. Similar to the componentDidMount, this method can be used // to perform DOM operations after the data has been updated.
    }

    componentWillUnmount() {
        // Called before the component is removed from the DOM. This method can be beneficial when
        // needing to perform clean up operations, f.e. removing any timers defined in
        // componentDidMount.
    }

    render() {
        let message = "undefined"
        if (this.context.isAuthenticated) {
            message = "true";
        } else {
            message = "false";
        }
        return (
            <div>
                {this.state.text}
                <ul>
                    <li><Link to='/login'>Login</Link></li>
                    <li><Link to='/logout'>Logout</Link></li>
                    <li><Link to='/signup'>Sign up</Link></li>
                    <li><Link to='/inbox'>Inbox</Link></li>
                    <li><a href='http://80.220.232.248'>Here is</a></li>
                </ul>
                <div>{this.state.date}</div>
                <div>{this.props.store}</div>
                <div>{this.props.aDefaultProp}</div>
                <div>{message}</div>
            </div>
        );
    }
}

module.exports = Template;
