//
// Graph
//
import React from 'react';
import d3    from 'd3';
import Resizable from 'react-component-resizable';

class Graph extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width:  0,
            height: 0
        }
    }

    onResize(resizeAttributes) {
        const {width, height}  = resizeAttributes;
        this.setState({width: width, height: height})
    }

    componentDidMount () {
    }

    render() {
        const {width, height} = this.state;
        const linearScaleX = d3.scale.linear()
        .domain([0, 63])
        .range([0, width]);

        const linearScaleY = d3.scale.linear()
        .domain([-256, +256])
        .range([390, 10]);

        // d3 line accessor function
        const lineFunction = d3.svg.line()
        .x((d) => {
            return linearScaleX(d.x);
        })
        .y((d) =>{
            return linearScaleY(d.y);
        })
        .interpolate('cardinal');

//        const data = this.props.data;
        const data = [
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 100, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0
            ];
        const circles = [];
        const points = [];
        data.map((y, x) => {
            const point = {x: x, y: y};
            points.push(point);
            const circle = (<circle key={x}  cx={linearScaleX(x)} cy={linearScaleY(y)} r="3" />);
            circles.push(circle);
            return {point: point, circle: circle};
        });

        return (
            <Resizable onResize={(resizeAttributes) => {
                    this.onResize(resizeAttributes);
            }}>
                <svg width='100%' height='400'>
                    <g strokeWidth='3' stroke='orange' fill='none'>
                        <path ref='path' d={lineFunction(points)}/>
                    </g>
                    <g stroke='red' fill='red'>
                        {circles}
                    </g>
                </svg>
                <h1>{width + ' x ' + height}</h1>
            </Resizable>
        );
    }
}

Graph.propTypes = {};
Graph.defaultProps = {};

module.exports = Graph;
