//
// Eight buttons with indicator LEDs
//
import React from 'react';
import { Button, Grid, Row, Col, Well } from 'react-bootstrap';

// FIXME: loader does not work
//const img    = require("url-loader?mimetype=image/png!./led.png");

class ButtonWithLed extends React.Component {
    constructor(props) {
        super(props);
        this.state = props;
    }

    onClick(event) {
        // Call a function in the parent passed in props
        // See: http://stackoverflow.com/questions/22639534/pass-props-to-parent-component-in-react-js
        this.props.onClick(this.props.buttonId);
    }

    render() {
        const style = {
            width: '12.5%',
            float: 'left'
        };
        // FIXME: There must be a better way to make a computed const!
        const bsStyle = (() => {
            if (this.props.button === 'on') {
                return 'danger';
            } else {
                return 'primary';
            }
        });
        // FIXME The above jscs error is a bug in jscs
        return (
            <div key={this.props.key} className='text-center' style={style}>
                <div style={{width: '100%'}}>
                   <img src={'assets/led.png'} alt={this.props.label} style={{paddingBottom: '20px'}} width='55px'/>
                </div>
                <Button  bsSize='small' bsStyle={bsStyle()} onClick={(event) => this.onClick(event)}>
                    {this.props.lable}
                </Button>
            </div>
        );
    }
}
ButtonWithLed.propTypes = {};
ButtonWithLed.defaultProps = {};

class ButtonPanel8 extends React.Component {
    constructor(props) {
        super(props);
        this.state = props;
    }

    onClick(event) {
        // Call a function in the parent passed in props
        // See: http://stackoverflow.com/questions/22639534/pass-props-to-parent-component-in-react-js
        this.props.onClick(event);
    }

    componentDidMount () {
    }

    render () {
        const buts = [
            {buttonId: 0, led: "off", button: "off", label: "GPIO 0" },
            {buttonId: 0, led: "off", button: "off", label: "GPIO 0" },
            {buttonId: 0, led: "off", button: "off", label: "GPIO 0" },
            {buttonId: 0, led: "off", button: "off", label: "GPIO 0" },
            {buttonId: 0, led: "off", button: "off", label: "GPIO 0" },
            {buttonId: 0, led: "off", button: "off", label: "GPIO 0" },
            {buttonId: 0, led: "off", button: "off", label: "GPIO 0" },
            {buttonId: 0, led: "off", button: "off", label: "GPIO 0" }
        ];
        //this.props.config.map((b, index) => {
        const buttons = [];
        buts.map((b, index) => {
            buttons.push(<ButtonWithLed key={index++}
                                        buttonId={b.buttonId}
                                        led={b.led}
                                        button={b.button}
                                        lable={b.label}
                                        width='12.5%'
                                        onClick={(event) => this.onClick(event)}/>);
        });
        return (
            <Row>
                {buttons}
            </Row>
        );
    }
}

ButtonPanel8.propTypes = {};
ButtonPanel8.defaultProps = {};

module.exports = ButtonPanel8;
