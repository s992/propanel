import THREE from 'three';

// FIXME: Get webpack image loading working
const imageSrc = 'textures/crate.gif';

class Cube {
    constructor(options) {
        this.width = options.width;
        this.height = options.height;
        this.scene = null;
        this.camera = null;
        this.mesh = null;
        this.renderer = null;
    }

    init() {
        this.renderer = new THREE.WebGLRenderer({alpha: true});
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(100, 100);
        const elem = document.getElementById('here');
        elem.appendChild(this.renderer.domElement);

        this.camera = new THREE.PerspectiveCamera(70, 100 / 100, 1, 1000);
        this.camera.position.z = 300;

        this.scene = new THREE.Scene();

        const geometry = new THREE.BoxGeometry(200, 200, 200);
        const texture = THREE.ImageUtils.loadTexture(imageSrc);
        //texture.anisotropy = renderer.getMaxAnisotropy();
        const material = new THREE.MeshBasicMaterial({ map: texture });
        this.mesh = new THREE.Mesh(geometry, material);
        this.scene.add(this.mesh);

        this.renderer.render(this.scene, this.camera);
    }

    resize(width, height) {
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(width, height);
    }

    animate() {
        requestAnimationFrame(this.animate.bind(this));

        this.mesh.rotation.x += 0.005;
        this.mesh.rotation.y += 0.01;

        this.renderer.render(this.scene, this.camera);
    }
}

module.exports = Cube;
