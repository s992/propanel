import React from 'react';
import { Link } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import {Nav, NavBrand, Navbar, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';

import About from './about.jsx';

function injector () {
    console.log("++++++++++++++++++++++++++++ CLIENT SIDE INJECTOR HERE +++++++++++++++++++");
}

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        injector: injector
    }

    static childContextTypes = {
        isAuthenticated: React.PropTypes.bool
    }

    getChildContext () {
        return {isAuthenticated: false};
    }

    render () {
        // Clone all the children of this component adding a prop to each
        var childrenWithProps = React.Children.map(this.props.children, function(child) {
            return React.cloneElement(child, { store: "Da Store"});
        });

        return (
            <html>
                <head>
                    <meta charSet='UTF-8'/>
                    <title>Propanel</title>
                    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
                    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
                    <link rel='stylesheet' type='text/css' href='css/bootswatch_com_cyborg_bootstrap.min.css' media='screen' />
                </head>
                <body>
                    <Navbar>
                        <NavBrand>
                            <Link to="/">2π.net</Link>
                        </NavBrand>
                        <Nav>
                            <LinkContainer to="/login">
                                <NavItem eventKey={1}>Login</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/inbox">
                                <NavItem eventKey={2}>Inbox</NavItem>
                            </LinkContainer>
                            <NavDropdown eventKey={3} title='Dropdown' id='basic-nav-dropdown'>
                                <MenuItem eventKey='1'>Action</MenuItem>
                                <MenuItem eventKey='2'>Another action</MenuItem>
                                <MenuItem eventKey='3'>Something else here</MenuItem>
                                <MenuItem divider />
                                <MenuItem eventKey='4'><About/></MenuItem>
                            </NavDropdown>
                        </Nav>
                    </Navbar>
                    {/*
                    next we replace `<Child>` with `this.props.children`
                    the router will figure out the children for us
                    */}
                    {childrenWithProps}
                    <script src='assets/bundle.js'></script>
                </body>
            </html>
        );
    }
}
module.exports = App;
