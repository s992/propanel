import React from 'react';
import { Link,  History }  from 'react-router';
import { form, Input, option, ButtonInput, Panel, Grid, Col } from 'react-bootstrap';

import auth from '../auth.jsx';

const Login = React.createClass({
    mixins: [History],

    getInitialState () {
        return {
            error: false
        };
    },

    handleSubmit (event) {
        event.preventDefault();

        const username = this.refs.username.getValue();
        const email    = this.refs.email.getValue();
        const password = this.refs.password.getValue();

        auth.login(username, password, (loggedIn) => {
            if (!loggedIn) {
                return this.setState({ error: true });
            }
            const { location } = this.props;

            if (location.state && location.state.nextPathname) {
                this.history.replaceState(null, location.state.nextPathname);
            } else {
                this.history.replaceState(null, '/');
            }
        });
    },

    render () {
        return (
            <Grid fluid>
                <Col md={6} mdOffset={3}>
                    <Panel header='Login:'>
                        <form onSubmit={this.handleSubmit}>
                            <Input ref="username" type='text'     label='User name'     placeholder='guest' defaultValue='guest'/>
                            <Input ref="email"    type='email'    label='Email Address' placeholder='guest@xn--2-umb.net' defaultValue='guest@xn--2-umb.net'/>
                            <Input ref="password" type='password' label='Password'/>
(hint: Password1)
                            <ButtonInput type='submit' value='Login' />
                            {this.state.error && (
                                <p>Bad login information</p>
                            )}
                        </form>
                        Don't have an accout? Sign up <Link to={'/signup'}>here</Link>
                    </Panel>
                </Col>
            </Grid>
        );
    }
})

module.exports = Login;
