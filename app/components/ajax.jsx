function httpRequest(url, cb) {
    const xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = () => {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                cb(null, xmlhttp.responseText);
            } else {
                cb (xmlhttp.status, null);
            }
        }
    };
    xmlhttp.open('GET', url, true);
    xmlhttp.send();
}

module.exports = httpRequest;
