//
// Three test
//
import React     from 'react';
import reactDOM  from 'react-dom';
import Resizable from 'react-component-resizable';

import Cube from './cube.jsx';

class ThreeTest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width:  0,
            height: 0
        }
    }

    onResize(resizeAttributes) {
        const {width, height}  = resizeAttributes;
        this.cube.resize(width, 400);
        this.setState({width: width, height: height})
    }

    componentDidMount () {
        this.cube = new Cube({width: 400, heigh: 400});
        this.cube.init();
        this.cube.animate();
    }

    componentDidUpdate() {
        const el = reactDOM.findDOMNode(this);
        //this.cube.resize(el.offsetWidth, this.props.height);
    }

    render() {
        const {width, height} = this.state;
        return (
            <Resizable onResize={(resizeAttributes) => {
                    this.onResize(resizeAttributes);
            }}>
                <div>
                    <div id='here' width='100%' height='400'>
                    </div>
                </div>
                <h1>{width + ' x ' + height}</h1>
            </Resizable>
        );
    }
}

ThreeTest.propTypes = {};
ThreeTest.defaultProps = {};

module.exports = ThreeTest;
