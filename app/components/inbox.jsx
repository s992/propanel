import React from 'react';

class InBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {count: props.initialCount};
    }

    render () {
        console.log("Renedering: Inbox");
        return <h2>Inbox</h2>;
    }
}

module.exports = InBox;
