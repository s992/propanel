import React from 'react';
import {Modal, ToolTip, Button, Popover, OverlayTrigger } from 'react-bootstrap';

class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            fart: 42
        };
    }

    close() {
        console.log("Model:  close");
        this.setState({
            showModal: false
        });
    }

    open() {
        console.log("Model:  open", this.state);
        this.setState({
            showModal: true
        });
    }

    render() {
//        const popover = <Popover title='popover'>very popover. such engagement</Popover>;
//        const toolTip = <ToolTip>wow.</Tooltip>;
        return (
            <div>
                <div onClick = {() => {this.open()}}>
                  About
                </div>
                <Modal show = {this.state.showModal} onHide = {() => this.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Propanel</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        An experimental control panel web application by heater.
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={() => this.close}>
                          Close
                       </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

module.exports = About;
