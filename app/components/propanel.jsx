import React from 'react';

import { Grid, Col, Row, Well, Panel, ListGroup, ListGroupItem, Tabs, Tab } from 'react-bootstrap';

import Graph        from './graph.jsx'
import ButtonPanel8 from './button-panel.jsx';
import Terminal     from './terminal.jsx';
import ThreeTest    from './three-test.jsx';
import Template     from './template.jsx';

class ProPanel extends React.Component {
    constructor(props) {
        console.log("App constructor");
        super(props);
        this.state = {
            tabKey: 1
        };
    }

    componentWillMount () {
    }

    handleTabSelect(tabKey) {
        //alert('selected ' + tabKey);
        this.setState({tabKey});
    }

    render () {
        console.log("propanel.render: With", this.props.store);
        return (
            <div>
                <br/>
                <Grid fluid>
                        <Col md={2}>
                            <h1>Propanel</h1>
                            <p className='lead'>An experimental control panel.</p>
                            <Panel collapsible defaultExpanded header='Signal:'>
                                <ListGroup fill>
                                    <ListGroupItem>Temp</ListGroupItem>
                                    <ListGroupItem>Pressure</ListGroupItem>
                                    <ListGroupItem>&hellip;</ListGroupItem>
                                </ListGroup>
                            </Panel>
                        <Template store={this.props.store}/>
                        </Col>
                        <Col md={8}>
                          <Tabs activeKey={this.state.tabKey} onSelect={this.handleTabSelect.bind(this)} animation={false}>
                            <Tab eventKey = {1} title = 'Graph 1'>
                                <Well>
                                    <Graph/>
                                </Well>
                            </Tab>
                            <Tab eventKey = {2} title = 'Graph 2'>
                                <Well>
                                  <Graph/>
                                </Well>
                            </Tab>
                            <Tab eventKey = {3} title = 'Buttons'>
                                <Well>
                                    <ButtonPanel8/>
                                </Well>
                            </Tab>
                            <Tab eventKey = {4} title = 'Terminal'>
                                <Well>
                                    <Terminal/>
                                </Well>
                            </Tab>
                            <Tab eventKey = {5} title = '3D'>
                                <Well>
                                    <ThreeTest/>
                                </Well>
                            </Tab>
                            <Tab eventKey = {6} title = 'Template'>
                                <Well>
                                    <Template store={this.props.store}/>
                                </Well>
                            </Tab>
                          </Tabs>
                        </Col>
                        <Col md={2}>
                        </Col>
                </Grid>
            </div>
        );
    }
}

module.exports = ProPanel;
