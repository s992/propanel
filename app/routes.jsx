import React  from 'react';

import { Route, IndexRoute } from 'react-router';

import App      from './components/app.jsx';
import ProPanel from './components/propanel.jsx';
import Signup   from './components/signup.jsx';
import Login    from './components/login.jsx';
import Logout   from './components/logout.jsx';
import InBox    from './components/inbox.jsx';
import NotFound from './components/notfound.jsx';

import auth     from './auth.jsx';

function requireAuth(nextState, replaceState) {
    console.log("requireAuth", nextState);
    if (typeof localStorage === 'undefined')
    {
        // FIXME: We really need server side auth check here.
        // We are runing on the server, may want to do something here.
        replaceState({ nextPathname: nextState.location.pathname }, '/login');

    } else {
        if (!auth.loggedIn()) {
            replaceState({ nextPathname: nextState.location.pathname }, '/login');
        }
    }
}

const routes = (
    <Route path='/' name='root' component={App}>
        <IndexRoute           component={ProPanel} onEnter={requireAuth}/>
        <Route path='signup'  component={Signup}/>
        <Route path='login'   component={Login}/>
        <Route path='logout'  component={Logout}   onEnter={requireAuth}/>
        <Route path='inbox'   component={InBox}    onEnter={requireAuth}/>
        <Route path='*'       component={NotFound}/>
    </Route>
);

module.exports = routes;
