import request from 'superagent';

module.exports = {
    login (username, password, cb) {
        cb = arguments[arguments.length - 1];
        if (localStorage.token) {
            if (cb) {
                cb(true);
            }
            this.onChange(true);
            return;
        }
        console.log('login: No token in localStorage');

        request
            .post('/login')
            .send({ username: username, password: password })
            .set('X-API-Key', 'foobar')
            .set('Accept', 'application/json')
            .end(function (err, res) {
                // Calling the end function will send the request
                // FIXME: This error testing is a mess
                if (err) {
                    console.log('login: Error', err);
                    if (cb) {
                        cb(false);
                    }
                    this.onChange(false);
                    return;
                }
                if (res.body.token) {
                    localStorage.token = res.body.token;
                    if (cb) {
                        cb(true);
                    }
                    this.onChange(true);
                } else {
                    console.log('login: No token in response');
                    if (cb) {
                        cb(false);
                    }
                    this.onChange(false);
                }
            }.bind(this));
    },

    getToken () {
        return localStorage.token;
    },

    logout (cb) {
        delete localStorage.token;
        if (cb) {
            cb();
        }
        this.onChange(false);
    },

    loggedIn () {
        return !!localStorage.token;
    },

    onChange () {
    }
};
