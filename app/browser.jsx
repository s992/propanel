import BabelPolyfill from "babel-polyfill";
import React from 'react'
import { render } from 'react-dom'
import { Router } from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import routes   from './routes.jsx';


// See react-router instructions here:
// https://github.com/rackt/react-router/blob/master/docs/Introduction.md

// Render a <Router> with some <Route>s.
// It does all the fancy routing stuff for us.
// See: transferring props to children in 1.0.0 https://github.com/rackt/react-router/issues/1531
render((
    <Router history={createBrowserHistory()}>
        {routes}
    </Router>
), document);

// Uncomment one of the following lines to see error handling
// require('unknown-module')
// a = /regex/q

if (module.hot) {
    module.hot.accept();
    module.hot.dispose(function() {
        // No idea what dispose is for
    });
}

